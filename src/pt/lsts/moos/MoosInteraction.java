package pt.lsts.moos;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JPopupMenu;

import pt.lsts.neptus.console.ConsoleInteraction;
import pt.lsts.neptus.i18n.I18n;
import pt.lsts.neptus.plugins.PluginDescription;
import pt.lsts.neptus.renderer2d.StateRenderer2D;
import pt.lsts.neptus.types.coord.LocationType;
import pt.lsts.neptus.types.mission.MissionType;

@PluginDescription
public class MoosInteraction extends ConsoleInteraction {
	
	LocationType mousePos = null;
	ArrayList<LocationType> markers = new ArrayList<>();
	
	@Override
	public void cleanInteraction() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void initInteraction() {
	    // TODO Auto-generated method stub
	}
	
	@Override
	public void paintInteraction(Graphics2D g, StateRenderer2D source) {
	    
	    g.setTransform(new AffineTransform());
	    
	    if (mousePos != null) {
	        MissionType mission = getConsole().getMission();
	        
	        if (mission != null) {
	            synchronized (markers) {
	                for (LocationType l : markers) {
	                    double[] offsets = l.getOffsetFrom(mission.getHomeRef());
	                    Point2D pt = source.getScreenPosition(l);
	                    g.setColor(Color.red.darker().darker());
	                    g.fill(new Ellipse2D.Double(pt.getX()-2, pt.getY()-2, 4, 4));
	                    g.setColor(Color.BLACK);
	                    String coords = String.format("(%3.01f, %3.01f)", offsets[1], offsets[0]);
	                    g.drawString(coords, (int)(pt.getX()+3), (int) (pt.getY()+10));
	                }
                }
	            
	            double[] offsets = mousePos.getOffsetFrom(mission.getHomeRef());
	            
	            g.setColor(Color.blue.darker().darker());
	            String coords = String.format("X=%3.01f Y=%3.01f", offsets[1], offsets[0]);
	            g.drawString(coords, 10, 20);
                g.drawString(String.format("Home: %.8f, %.8f", mission.getHomeRef().getLatitudeDegs(), mission.getHomeRef().getLongitudeDegs()), 10, 40);
	        }
	    }
	    
	    
	}
	
	@Override
	public void mouseMoved(MouseEvent event, StateRenderer2D source) {
		mousePos = source.getRealWorldLocation(event.getPoint());
	}
	
	@Override
	public void mouseClicked(MouseEvent event, StateRenderer2D source) {
	    if (event.getButton() == MouseEvent.BUTTON3) {
	        JPopupMenu popup = rightClickMenu(source.getRealWorldLocation(event.getPoint()));
	        popup.show(source, event.getX(), event.getY());
	    }
	    else if (event.getClickCount() == 2) {
            synchronized (markers) {
                markers.add(source.getRealWorldLocation(event.getPoint()));
            }
	    }
	}	
	
	private JPopupMenu rightClickMenu(LocationType clickLocation) {
	    JPopupMenu popup = new JPopupMenu();
	    popup.add(I18n.text("Clear markers")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (markers) {
                    markers.clear();    
                }
                
            }
        });
	    return popup;	    
	}
}
