﻿#Neptus-MOOS plug-in
----
##Description
This Neptus package provides some utilities to make interaction with MOOS systems easier.

###MoosInteraction
This plug-in allows displaying of coordinates as North/East offsets relative to the mission home reference. Used for creating MOOS-IVP Waypoint behaviors.

###MoosConnection
This plug-in adds a menu Advanced>MOOS that allows connecting to a MOOSDB and monitor its data. Moreover it allows sending of data to the same MOOSDB.

###License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as  published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 